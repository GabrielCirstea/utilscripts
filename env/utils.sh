#/usr/bin/bash

# util function to be sourced

# upload file to 0x0.st
# store the url for later use
# $1 - file path
share_null()
{
	file=${1?"file path required"}
	url=$(curl -F"file=@$file" http://0x0.st)
	echo "$(date) $file: $url" >> ~/.cache/share_null.log
	echo "$url"
}
