#!/bin/sh

# create an overlay mountpoint and delete a target file/directory to "hidde" it

if [ $# -lt 1 ]
then
	echo "Usage: $0 <path>"
fi

target=${1?"A file/folder to hidde is required"}
target_parent=$(dirname "$target")
mountpoint="$target_parent"

UPPERDIR="/tmp/uppder"
WORKDIR="/tmp/work"

if ! mount -t overlay overlay \
	-o lowerdir="$target_parent",upperdir="$UPPERDIR",workdir="$WORKDIR" \
	"$mountpoint"
then
	echo "Could not create the overlay over $mountpoint"
	exit 1
fi

rm -rf "$target"
