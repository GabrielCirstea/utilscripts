#!/bin/sh

# print usage
# $1 name of script
usage() {
	echo "$1: Connect to my VMs"
	echo "Interactive mode, requires fzf"
	exit 1;
}

# list, select and connect to a virtual machine via ssh
interactive() {
	domain=$(virsh list --name |  fzf)
	ip_addr=$( echo "$domain" \
		| xargs virsh domifaddr | grep "ipv4" | awk '{print $NF}' | cut -d "/" -f1)
	echo "IP: $ip_addr"
	[ -z "$ip_addr" ] && return 1
	echo "Description:"
	virsh desc "$domain"
	echo -n "What user, sir?: "
	read user
	[ -z "$user" ] && return 1
	ssh $user@$ip_addr && return 0
}

if ! command -v fzf > /dev/null
then
	echo "ERROR: fzf not found"
	exit 1
fi

if [ $# -gt 0 ] 
then 
	case "$1" in
		"-h") usage $0 ;;
	esac
fi

interactive
