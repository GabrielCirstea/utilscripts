#!/bin/sh
# quick hide a directory
# create a overlayfs mount point above the direcotry

uid=$(id -u)
gid=$(id -g)
scripts_dir="~/Scripts"
unshare -rm /usr/bin/bash -c "sh $scripts_dir/env/mount-hide.sh ~/.config/nvim && unshare -um --map-user=$uid --map-group=$gid"
