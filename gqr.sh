#!/bin/sh

# generate a qr code using zint or qrencode

# Usage: gqr [-aipuc] content/url

usage()
{
	echo "Usage: $0 [-aipuc] content/url" >&2
	cat << EOF
	-a	address
	-i	interface
	-p	port
	-u	full url
	-c	get the content from clipboard
	-t	text mode, display the qrcode in terminal (uses qrencode)
EOF
}

OUT_FILE="$HOME/Pictures/qr.png"
IMG_VIEW=sxiv
# set to 1 if you want to open the image and if the img viewer is found
IMG_SET=1

# check if zint and sxiv (or other set image viewer is installed
if ! command -v zint > /dev/null
then
	echo "ERROR: zint not found"
	ZINT_404=1
fi

if ! command -v "$IMG_VIEW" > /dev/null
then
	echo "ERROR: $IMG_VIEW not found"
	IMG_SET=0
fi

if [ $# -lt 1 ]
then
	usage
	exit 1
fi

while getopts ":a:i:p:u:ct" o; do
    case "${o}" in
        a)
            addr=${OPTARG}
            ;;
        i)
            interface=${OPTARG}
            ;;
        p)
			port=${OPTARG}
            ;;
		u)
			url=${OPTARG}
			;;
		c)
			url=$(xclip -selection clipboard -o)
			;;
		t)
			ZINT_404=1
			;;
		*) echo Option $o not supported ;;
    esac
done
shift $((OPTIND-1))

if [ "$interface" ]
then
	addr=$(ip a show "$interface" | grep "inet " \
		| awk '{print $2}' | cut -d'/' -f1)
fi

[ "$addr" ] && content="http://$addr"

[ "$port" ] && content="$content:$port"

[ "$url" ] && content="$url"

# use zint or grencode as selected
if [ -z "$ZINT_404" ]
then
	zint -b QRCode --scale=3 -d "${content?no arg given}" -o "$OUT_FILE" || echo "Fail"
else
	echo "${content?no arg given}" | qrencode -t ansiutf8
fi

# check if possible to print the qrcode
if [ -z "$ZINT_404" ] && [ "$IMG_SET" = 1 ]
then
	$IMG_VIEW "$OUT_FILE"
fi
