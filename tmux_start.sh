#!/bin/sh
# create a tmux session with the specified name and launch a program in there

# learn command lline argumets from 
# > https://www.grymoire.com/Unix/Sh.html

usage() {
	echo $(basename "$0"): ERROR: "$@" 1>&2
	echo usage: $(basename "$0") ' [-n session_name] [-d cirectory] [-c command]'
   	'[file ...]' 1>&2
	exit 1
}

# must give some arguments
[ $# -eq 0 ] && usage "Not arguments"

set -- $(getopt "n:d:c:" "$@") || usage ""

while :
do
        case "$1" in
        -n) shift; NAME=${1?"Must give a name"};;
        -d) shift; DIR=${1-"$PWD"};;
        -c) shift; COMMAND="$1";;
        --) break;;
        esac
        shift
done
shift # get rid of --


cd "$DIR"
tmux new -s "${NAME?"Must give a name"}" -d
tmux send-keys -t "$NAME" "$COMMAND" Enter
