#!/usr/bin/bash

# primeste argument locatia unui fisier
# pe care il va pune ca sursa pt exemplul de md live preview de pe https://github.com/tanabe/markdown-live-preview

# verifica daca a primit argument
if [ $# -ne 1 ];
then
	echo "Please use 'script path_to_source' !"
	echo $#
	exit 1;
fi

LOCAL_PATH="/home/$USER/Scripts/markdown-rander";
NEW_FILE="new.html";

# concateneaza partiile necesare la content
cat "${LOCAL_PATH}/first-part.txt" > "${LOCAL_PATH}/${NEW_FILE}";
cat "$1" >> "${LOCAL_PATH}/${NEW_FILE}";
cat "${LOCAL_PATH}/last-part.txt" >> "${LOCAL_PATH}/${NEW_FILE}";

# deschide fisierul in firefox
firefox "${LOCAL_PATH}/${NEW_FILE}";
