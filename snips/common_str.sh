#!/bin/sh

# Echo the first common part of the strings

str1="${1?Require a first argument}"
str2="${2?Require a second argument}"
len=${#2}
[ -z "$str1" ] || [ -z "$str2" ] && return 1
#s=$(expr substr "$str2" 1 "$len")
s=$(echo "$str2" | cut -c1-"$len")
while ! expr "$str1" : "$s" > /dev/null
do
	len=$((len - 1))
	#s=$(expr substr "$str2" 1 "$len")
	s=$(echo "$str2" | cut -c1-"$len")
done
echo "$s"
