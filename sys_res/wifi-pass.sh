#!/bin/sh

# Get the saved wi-fi passwords
# Requires root

DIR="/etc/NetworkManager/system-connections"

#checks
[ ! -d "$DIR" ] && { echo "Dir $DIR not found"; exit 1; }

file_name=$(find "$DIR" -type f -exec basename {} \; | fzf)

sudo grep "^psk=" "$DIR/$file_name"
