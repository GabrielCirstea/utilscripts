#!/bin/sh
# Print a warning if any disk is more
# than 90% full.
df | tr -d '%' | awk '
# only look at lines where the first field contains a "/"
$1 ~ /\// {	if ($5 > 90) {
		printf("Warning, disk %s is %4.2f%% full\n",$6,$5);
	}
}'
