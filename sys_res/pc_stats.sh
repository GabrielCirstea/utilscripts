#!/usr/bin/bash

# afiseaza procesele ce folosesc cea mai multe memorie
echo "Most intens processes mem:"
ps axch -o cmd,%mem --sort=-%mem | head

echo "Most intens processes cpu:"
ps axch -o cmd,%cpu --sort=-%cpu | head

# afiseaza meemoria ocupata/totala
echo "Mem:"
free -h | awk '/^Mem:/ {print $3 "/"$2}'
