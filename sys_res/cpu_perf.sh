#!/bin/sh

# Change the cpu performance mode

echo "scaling max_freq:"
cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_max_freq

echo -n "energy_performance_preference: "
cat /sys/devices/system/cpu/cpu0/cpufreq/energy_performance_preference

echo -n "energy_performance_available_preferences: "
cat /sys/devices/system/cpu/cpu0/cpufreq/energy_performance_available_preferences



echo "Change to performance mode?"
read ans
case "$ans" in
	"yes" | "y")
		echo "performance" | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
		;;
	"no" | "n" | *) echo "OK, nothing done" ;;
esac

