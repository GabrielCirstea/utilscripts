#!/bin/sh

# Try to write a scrip that will set the brightness of the screen

SYS_BRIGTH_FILE="/sys/class/backlight/intel_backlight/brightness"

show_bright()
{
	cat "$SYS_BRIGTH_FILE"
}

# $1 - value between 0 and 180 ?
set_brigth()
{
	level=${1?'Provide a level please'}
	echo "$level" | sudo tee "$SYS_BRIGTH_FILE"
}

if [ $# -eq 0 ]
then
	show_bright
else
	set_brigth $1
fi
