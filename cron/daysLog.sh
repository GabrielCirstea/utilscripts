#!/usr/bin/bash

# record daily time on PC
LOG_FILE="$HOME/Scripts/daysLogs.txt"
last_line=""
IFS=" "	# delimiter
[[ -f "$LOG_FILE" ]] && last_line=$(tail -n 1 $LOG_FILE) && read -ra ARR <<< "$last_line"

day=$(date +%d_%m_%Y)
minutes=0
[[ $day == ${ARR[0]} ]] && minutes=${ARR[1]}
(( minutes++ ))

[[ $day == ${ARR[0]} ]] && sed -i "s/${ARR[0]} ${ARR[1]}/$day $minutes/" $LOG_FILE || echo "$day $minutes" >> $LOG_FILE
