#!/bin/sh

# get the logs from
LOG_DIR=""
# put the tarball in
COMP_DIR=""

old_pwd=$(pwd)
mo=$(date +%m)
case $mo in
	"0[2-9]" | "1[12]") mo=$((mo-1)) ;;
	"01") mo=11 ;;
esac
mo=$(printf "%0*d" 2 $mo)
date="$(date +%Y)-$mo"
cd "$LOG_DIR" || echo "Cannot cd into $LOG_DIR"
tar czf "$COMP_DIR"/"res-rec-$date.tar.gz" $(find -name "resources-$date-*.log")
cd "$old_pwd"
