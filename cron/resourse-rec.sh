#!/bin/sh

# print the current used resources

# target directory
LOG_DIR=""

resources()
{
	echo "Who's online:"
	w

	echo "Most intens processes mem:"
	ps axch -o cmd,%mem --sort=-%mem | head

	echo "Most intens processes cpu:"
	ps axch -o cmd,%cpu --sort=-%cpu | head

	echo "Mem:"
	free -h

	echo "I/O status:"
	iostat
}

[ -d "$LOG_DIR" ] || mkdir "$LOG_DIR"
resources > "$LOG_DIR/resources-$(date +%F-%H).log"
