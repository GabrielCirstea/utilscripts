<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="content-language" content="en">
    <meta http-equiv="content-script-type" content="text/javascript">
    <meta http-equiv="content-style-type" content="text/css">
    <meta name="description" content="This is the online markdown editor with live preview.">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/github-markdown.css">
    <link rel="icon" type="image/png" href="favicon.png">

    <script type="text/javascript" src="js/jquery-1.6.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.autosize-min.js"></script>
    <script type="text/javascript" src="js/markdown.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

    <title>Markdown Live Preview</title>
  </head>
  <body>

    <div id="header">
      <h1><a href="/">Markdown Live Preview</a></h1>
    </div>

    <div id="container">
      <div id="content" class="section">

        <div id="edit" class="mode">
          <div class="content">
            <textarea id="markdown">

