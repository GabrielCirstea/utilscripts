#!/bin/sh

# alias pentur rulate pandoc din container
# https://github.com/pandoc/dockerfiles

docker run --rm --volume "`pwd`:/data" --user `id -u`:`id -g` pandoc/core:latest "$@"
