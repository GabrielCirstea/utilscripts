#!/bin/sh

# list the different files from 2 directories
# use "long" option to display all differences - similar to `diff -r dir1 dir2`

verbose="false"

# colors
# end of color
CEND='\033[0m'
CSTART='\033['
# 31 - red, 1 - bold, 4 - underlined
CRED='\033[31;1;4m'
CBOLD='\033[1;4m'

usage() {
	echo "$0 {dir1} {dir2} [args]"
	echo "args: long, short"
}

# replace path prefix
# $1 - prefix to replace
# $2 - prefix to replace with
# $3 - path
path_prefix() {
	echo "$3" | sed "s|$1|$2|"
}

# show non existend file
# $1 - dir1 files that are in this directory and not in the other 1
# $2 - dir 2
no_in() {
	dir1="${1?'Dir1 path required'}"
	dir2="${2?'Dir2 path required'}"
	for f in $(find "$dir1" -type f)
	do
		f2=$(path_prefix $dir1 $dir2 $f)
		[ -f "$f2" ] || echo "$f"
	done
}

# show diff of files
# $1 - first directory to get the files from
# $2 - second directory to compare the files with
diffs() {
	dir1="${1?'Dir1 path required'}"
	dir2="${2?'Dir2 path required'}"

	for f in $(find "$dir1" -type f)
	do
		f2=$(path_prefix "$dir1" "$dir2" "$f")
		if [ -f "$f2" ]
		then
			out=$(diff "$f" "$f2")
			if [ "$out" ]
			then
				printf "$CBOLD"
				path_prefix "$dir1" "-" "$f"
				printf "$CEND"
				echo "$out"
			fi
		fi
	done
}

main() {
	echo "Files in $DIR1:"
	no_in "$DIR1" "$DIR2"
	echo "Files in $DIR2:"
	no_in "$DIR2" "$DIR1"
	[ "$verbose" = "true" ] && diffs "$DIR1" "$DIR2"
}

if [ $# -lt 2 ]
then
	usage
	exit 1
fi

DIR1="$1"
DIR2="$2"

shift 2
while [ "$1" ]
do
	case $1 in
		"short") verbose="false" ;;
		"long") verbose="true" ;;
	esac
	shift
done

main
