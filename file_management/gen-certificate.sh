#!/bin/sh

# openssl certificate helper
# small script to help with certificate generation, for now

days=365

usage()
{
	echo "Usage: $0 [opt]"
	cat << EOF
Options:
	-k	Key file
	-r	request file name (if not signing this will be the output)
	-c	conf file name (or extension file if signing)
	-C	CA cert
	-K	CA key
	-e	extension section name from conf file (extension file)
	-o	certificate to be created (only if signing)
	-S	sign the request file
EOF
	exit 1
}

# generate the request
# $1 - key
# $2 - request out name
# $3 - conf file
gen_req()
{
	key_path="$1"
	csr_out="$2"
	conf_file="$3"
	openssl req -new -out "$csr_out" -config "$conf_file" -key "$key_path"
}

sign_req()
{
	csr_in="$1"
	cert_out="$2"
	conf_file="$3"
	ext_name="$4"
	CA_file="$5"
	CA_key="$6"
	openssl x509 -req -days $days \
		-in "$csr_in" \
		-CA "$CA_file" -CAkey "$CA_key" -CAcreateserial \
		-out "$cert_out" \
		-extfile "$conf_file" -extensions "$ext_name"
}

sign_mode="false"

while getopts ":k:c:r:C:K:e:o:S" o; do
    case "${o}" in
        k)
            key_path=${OPTARG}
            ;;
        c)
            conf_file=${OPTARG}
            ;;
        r)
			csr_file=${OPTARG}
            ;;
        C)
			CA_file=${OPTARG}
            ;;
        K)
			CA_key=${OPTARG}
            ;;
        e)
			ext_name=${OPTARG}
            ;;
        o)
			out_cert=${OPTARG}
            ;;
        S)
			sign_mode="true"
            ;;
		*) echo Option $o not supported ;;
    esac
done
shift $((OPTIND-1))

if [ "$sign_mode" = "true" ]
then
	sign_req "$csr_file" "$out_cert" "$conf_file" "$ext_name" "$CA_file" "$CA_key"
else
	gen_req "$key_path" "$csr_file" "$conf_file"
fi
