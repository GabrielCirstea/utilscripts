#!/bin/sh

# alias pentur rulare mai rapida a scripturilor ce apeleaza repetat pandoc din
# container
# https://github.com/pandoc/dockerfiles

docker run --rm --volume "`pwd`:/data" --user `id -u`:`id -g` --entrypoint "/data/${1?'Must give a script name'}" pandoc/core:latest
