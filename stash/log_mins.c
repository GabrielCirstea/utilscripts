/* Log How many minutes I'm spending daily on PC
 * This script will simply read a file and update the counter on the current date
 * Is to be use as a cron job
 * No special compile flags: "gcc log_mins.c -o log" */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <string.h>

// create the file, and close it
int create_file(const char *path)
{
	FILE *f = fopen(path, "a");
	fclose(f);
	return 0;
}

int main(int argc, char **argv)
{
	char *mins_file = argc > 1 ? argv[1] : "./log.txt";
	create_file(mins_file);
	FILE *f = fopen(mins_file, "r+");
	if(!f) {
		perror("fopen");
		exit(1);
	}

	time_t time_ref = time(NULL);
	struct tm *today = localtime(&time_ref);

	int updated = 0;

	/* go over the file to search to the last log today, if not create another
	 * entry */
	while(!feof(f)) {
		long offset = ftell(f);
		int d, m, y, mins;
		// read the values, but ignore the last '\n'
		int n = fscanf(f, "%d_%d_%d %d%*[\n]", &d, &m, &y, &mins);
		if(n == -1 || n != 4) {
			printf("n=%d\n", n);
			perror("Fscanf");
			fprintf(stderr, "fscanf: not all vars read\n");
			break;
		}
		// if the line is today date update the mins value
		if( d == today->tm_mday &&
			m == today->tm_mon &&
			y == today->tm_year + 1900 ) {
			// printf("Gasit linia\n");
			++mins;
			// get back, at the begining of the line
			if(fseek(f, offset, SEEK_SET)) {
				perror("fseek");
				break;
			}
			char update[256];
			// rewrite all line
			snprintf(update, 255, "%d_%d_%d %d\n", d, m, y, mins);
			fflush(f);
			if(!fwrite(update, strlen(update), 1, f)){
				perror("fwrite");
			}
			updated = 1;
			break;
		}
	}

	// insert new day
	if(!updated) {
		fprintf(f, "%d_%d_%d 1\n", today->tm_mday, today->tm_mon, today->tm_year + 1900);
	}

	fclose(f);
	return 0;
}
