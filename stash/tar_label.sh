#!/bin/sh

# display the "LABEL" version of a container stored as tar file

dir=${1-"."}

for f in $dir/*.tar
do
	jsons=$(tar tf "$f" | grep "[0-9]\+.*\.json")
	cont="$(basename $f)"
	[ "$jsons" ] && echo -n "${cont%.*} " &&
	   	tar xf "$f" "$jsons" -O | jq .config.Labels.version
done
