#!/bin/sh

# create and start writing a new Zettel

# how big will this get?

readonly zett_dir="$HOME/Documents/zettelkasten"

# create a new zett with the tittle as date-time...
new_zet()
{
	echo "new Zettel"
	zettel="$(date +%Y%m%d%H%M).md"

	# if already there jump to writing
	file="$zett_dir/$zettel"
	[ -f "$file" ] && vim "$file" && exit 0

	echo "$zettel"

	printf "%s" "# ${zettel%.*} " >> "$zett_dir/$zettel"
	vim "$zett_dir/$zettel"
}

# search for a word in zettles and print it nicely
# prints the name of the file and the title/first line
# $1 - search term
format_search()
{
	res=$(grep -rIil "$1" "$zett_dir")
	for f in $res
	do
		printf '%s: %s\n' "$(basename $f)" "$(head -n1 $f)"
	done
}

alias fzfp="fzf --preview 'bat --style=numbers --color=always --line-range :500 {}'"

# search a zett containig a word ?
# $1 - the searching stuff
search_zet()
{
	[ -z "$1" ] && return
	# switch to the zett_dir - reuqired to the fzfp command
	cd "$zett_dir" || return 1
	res=$(grep -rIil "$1" "$zett_dir")
	name=$(format_search "$1" | cut -f1 -d":" | fzfp)
	[ -z "$name" ] && return
	vim ${name%:*}
}

[ -d "$zett_dir" ] || exit 1

case $1 in
	"") new_zet
		exit 0
		;;
	"-s" | "--search" ) 
		shift
		word="${*}"
		echo "Search: $word"
		search_zet "$word"
		exit 0
		;;
	# a somewhat quick browser for the zettles
	"-l" | "--list") echo list
		lf $zett_dir
		;;
esac
