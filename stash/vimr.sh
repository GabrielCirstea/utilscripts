#!/usr/bin/sh

# shorcut to connect to a vim server
# server created with `vim --servername $NAME`

FILE=${1?'require file name'}
# server name is $2 if it exist or "VIM"
SERVER_NAME=${2-"VIM"}
vim --servername $SERVER_NAME --remote $FILE
