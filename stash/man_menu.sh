#!/usr/bin/bash
# un meniu interactiv pentru man pages
 man -k . | dmenu -l 30 | awk '{print $1}' | xargs man -Tpdf | zathura -
