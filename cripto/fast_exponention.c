/* fast_exponention.c
 * Apply fast exponention algorithm on a given set of numbers
 * Arguments: Modulo Number^power [number^power ...]
 * Ex: 5 2^16 4^20
 * It will calculate (2^16)%5 and (4^20)%5 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// find the biggest power of 2 smaller then n
int get_power(int n)
{
	int p = 0;
	while ( (1 << p) < n) p++;
	// fa ceva cu rahatul asta de if
	if( (1 << p) > n)
		return p-1;
	return p;
}

void test() {
	int v[] = {1, 245 ,16, 22, 44, 32, 33, 2910};
	int i;
	for(i=0; i< sizeof(v)/sizeof(v[0]); ++i){
		printf("%d: %d\n", v[i], get_power(v[i]));
	}
}

struct Item{
	int number;
	int power;
};

int read_numbers(int n, char** s, struct Item *v)
{
	int i;
	for(i = 0; i < n; ++i){
		char *p = strtok(s[i], "^");
		if (p){
			v[i].number = atoi(p);
		} else {
			fprintf(stderr, "eroare parsare %s\n", s[i]);
			return -1;
		}

		p = strtok(NULL, "^");
		if(p){
			v[i].power = atoi(p);
		} else {
			fprintf(stderr, "eroare parsare %s\n", s[i]);
			return -1;
		}
	}
	return 0;
}

/* printeza calculele necesare pentru aflarea rezultatului final
 * @n - baza
 * @p - putere/exponentul
 * @m - modulul */
void print_exponention(struct Item item, int m)
{
	int p = item.power,
		n = item.number;
	printf("___(%d^%d)%%%d___\n", n, p, m);
	int power = get_power(p);
	int i;
	unsigned int total = 1;
	unsigned long int current = n;
	for(i = 0; i <= power; ++i){
		if(( p & (1<<i) )){
			printf("%d^%d %% %d = %u\n", n, (1<<i), m, current);
			total *= current;
			total %= m;
		}
		current *= current;
		current %= m;
	}

	printf("%d^%d %% %d = %u\n\n", n, p, m ,total);
}

/* print all the powers of 2 from fast exponention
 * @n - number
 * @p - exponen
 * @m - modul */
void print_2powers(struct Item item, int m)
{
	int p = item.power,
		n = item.number;
	printf("___(%d^%d)%%%d___\n", n, p, m);
	int power = get_power(p);
	int i;
	unsigned int current = n;
	for(i=0; i <= power; ++i){
		printf("%d^%d %% %d = %u\n", n, (1 << i), m, current%m);
		current *= current;
		current %= m;
	}
}

int main(int argc, char** argv)
{
	if(argc < 3){
		fprintf(stderr,"Usage: %s modul bas^pow ...\nNO SPACES near '^'\n", argv[0]);
		exit(1);
	}

	// o sa pregatim niste loc static
	int modul = atoi(argv[1]);
	struct Item items[100] = {0};
	read_numbers(argc - 2, argv + 2, items);
	int i;
	for(i=0; i<argc-2; ++i){
		print_2powers(items[i], modul);
		print_exponention(items[i], modul);
	}
}
