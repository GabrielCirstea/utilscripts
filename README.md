# Some scripts for some use cases

Just some script that I use sometimes.

## Description

* *mark-rand.sh* accept the file path as an argument and rander the .md file in
browser
* *man_menu.sh* is an interactiv menu for man pages
* *weather.sh* uses the wttr.in site to get basic weather info
* *daysLog.sh* count the number of minutes while on PC. It has to work as a cronjob
* *quickyta* is a quick youtube-dl for audio, made to be use in dmenu
* *vimr.sh* to easy open a file in a vim server. The server need to be created
before.
* *jobsig* send a signal to a job, mostly for "jobs -p %% | jobsig.sh -CONT"

And there are some scripts for resource usage, that I don't use yet.

### markdown-rander

It's a script that I cloned from github

[markdow-preview](https://github.com/tanabe/markdown-live-preview)

I made *mark-rand.sh* to use that script in order to be easy to view and write
mardown files
