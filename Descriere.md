# Scripturi bash

Detalii prinvind Scripturile pe care le fac in Bash.
Poate vor fi si urtile
___
* __mark-randr.sh__ primeste argument path-ul catre un fisier pe care il
deschide si interpreteaza in browser  
Probleme:
	* Daca browserul nu era deja deschis, terminalul il va porni si acesta va 
	ramane blocat pana la inchiderea browser-ului
* __pc-stats.sh__ afiseaza memoria folosita/libera, procesele care consuma cea 
mai multa memorie respectiv cel mai mult CPU
* __weather.sh__ _alias weather_ -  apeleaza curl wttr.in pentur a afisa vremea.
 Foloseste locatia _Giurgiu_ si se poate specifica un numar dupa apel pentru format._(weater 4)_
* __man_menu.sh__ afiseaza toate paginile de manual in dmenu si le deschide in 
format pdf in zathura
* __dayLogs.sh__ numara minutele pe PC in **dayLogs.txt**, trebuie folosit in cron(8)
* __quickyta.sh__ este un script care foloseste youtube-dl pentru a descarca
paertea audio dintr-un videoclip, direct intr-un folder setat. A fost facut pt dmenu

___

Se pare ca MX vine cu cron deja setat
