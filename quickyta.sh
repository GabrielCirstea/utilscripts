#!/bin/sh

# doawnload music from youtube or othe sites on a specific plase

# require to pass a link
[ $# -ne 1 ] && exit

(youtube-dl -f bestaudio -x --audio-format mp3 -o "$HOME/Music/%(title)s.%(ext)s" $1 && 
	notify-send -i ~/Poze/logo_app.svg  Download finished) ||
	# nu prea merge cu sau-ul ala aici
	notify-send -i ~/Poze/logo_app.svg  Shomething bad happend
