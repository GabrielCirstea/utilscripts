#!/bin/sh
# Send a signal to job or any program using kill

# a try to work easy with jobs
# the script is not that usefull
# it requiers the user to write the pid of the job and to put the signal as a
# parameter of the script

FLAGS=${1-"-9"}

read JOB_PID<&0

kill $FLAGS ${JOB_PID?}
