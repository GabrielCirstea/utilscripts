#!/usr/bin/sh
# A script to show the key bindings on my window manager
# The problem is that on awesom wm the keybidings are writen in a hard to understand
# layout

set -e

sed -n '/START_KEYS/,/END_KEYS/p' ~/.config/awesome/rc.lua |
   	sed "s/^\s*--.*//g" |
   	sed "s/^\s*//"
